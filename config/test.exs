import Config

# Only in tests, remove the complexity from the password hashing algorithm
config :bcrypt_elixir, :log_rounds, 1

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :pref_ui, PrefUiWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "38jM+RP2ogujnY8XJBAR/19I0GJXUXwf7oJoyUM9vS9dZCU6ez1kJTJE+E8w+iX7",
  server: false

# In test we don't send emails.
config :pref_ui, PrefUi.Mailer,
  adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

config :pref_engine, PrefEngine.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "pref_engine_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10
