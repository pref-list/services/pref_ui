defmodule PrefUiWeb.NewItemModalComponent do
  use PrefUiWeb, :live_component

  alias PrefEngine.{Item, ItemURL}
  alias PrefEngine.PrefList
  
  @impl true
  def update(assigns, socket) do
    # NOTE: shouldn't current_user be part of session_vt (or at least the server)?
    %{id: id, pref_id: pref_id, current_user: current_user} = assigns

    # NOTE: can initialize this with data from session server
    {:ok, socket
      |> assign(:dialogue_open, false)
      |> assign(:url_changeset, ItemURL.changeset(%ItemURL{}, %{}))
      |> assign(:id, id)
      |> assign(:pref_id, pref_id)
      |> assign(:current_user, current_user)
    }
  end

  ## Add Item ##
  def handle_event("open_dialogue", _data, socket) do
    {:noreply, assign(socket, :dialogue_open, true)}
  end

  @impl true 
  def handle_event("close_dialogue", _data, socket) do
    {:noreply, assign(socket, :dialogue_open, false)}
  end

  def handle_event("url_change", %{"item_url" => %{"url" => url}}, socket) do
    {:noreply, socket
      |> assign(:url_changeset,  ItemURL.changeset(%ItemURL{}, %{url: url}))
    }
  end

  # TODO: this should be in the controller, right?
  def handle_event("submit_form", _data, socket) do
    assigns = socket.assigns
    case Item.insert_url(
      assigns.current_user.id,
      Ecto.Changeset.get_field(assigns.url_changeset, :url),
      assigns.pref_id
    ) do
      {:ok, item_id} ->
        PrefList.add_at_start(assigns.pref_id, :good, item_id)

        # Cleanup
        {:noreply, socket
          |> assign(:dialogue_open, false)
          |> assign(:url_changeset,  ItemURL.changeset(%ItemURL{}, %{}))
        }
      {:error, url_changeset} ->
        {:noreply, assign(socket, :url_changeset, url_changeset)}
      x = {:error, _multi_name, _error, _multi_map} ->
        IO.inspect x
        #IO.puts "Fail"
        {:noreply, socket}
    end
  end
end
