defmodule PrefUiWeb.PageController do
  use PrefUiWeb, :controller
  alias PrefUiWeb.UserPreflistsController

  alias PrefEngine.Accounts.User

  @default_name "Preference List"

  def index(conn, _params) do
    user = conn.assigns.current_user

    if User.has_at_least_one_preflist(user) do
      redirect(conn, to: Routes.user_preflists_path(conn, :index))
    else
      UserPreflistsController.create(conn, %{
        "action" => "new_preflist",
        "pref_list" => %{"name" => @default_name}
      })
    end
  end
end
