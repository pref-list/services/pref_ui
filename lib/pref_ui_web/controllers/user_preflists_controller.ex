defmodule PrefUiWeb.UserPreflistsController do
  use PrefUiWeb, :controller

  #alias PrefEngine.Accounts.User
  alias PrefEngine.{PrefList, Repo}

  # TODO: use Router Helper instead to figure out path of the live view
  alias PrefUiWeb.PrefListLive
  #alias PrefUiWeb.Endpoint

  plug :assign_new_and_change_changesets

  def index(conn, _params) do
    user = conn.assigns.current_user
           |> Repo.preload([:pref_lists])

    render(conn, "index.html", preflists: user.pref_lists)
  end

  # NOTE: never used
  #defp new_preflist(conn, name) do
  #  user = conn.assigns.current_user
  #  PrefList.new_changeset(%PrefList{},
  #    %{user_id: user.id, name: name}
  #  )
  #  |> Repo.insert()
  #end

  def create(conn, %{"action" => "new_preflist"} = params) do
    %{"pref_list" => %{"name" => name}} = params

    case PrefList.new(conn.assigns.current_user.id, name) do
      {:ok, preflist} ->
        conn
        |> redirect(to: Routes.live_path(conn, PrefListLive, preflist.id))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "index.html", new_changeset: changeset)
    end
  end

  defp assign_new_and_change_changesets(conn, _opts) do
    user = conn.assigns.current_user
    name = "Unnamed List"
    new_changeset = PrefList.new_changeset(%PrefList{},
      %{user_id: user.id, name: name}
    )

    conn
    |> assign(:new_changeset, new_changeset)
  end
end
