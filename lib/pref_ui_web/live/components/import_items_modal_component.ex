defmodule PrefUiWeb.ImportItemsModalComponent do
  use PrefUiWeb, :live_component

  #alias PrefEngine.{Item, ItemURL, ItemName, ItemDescription, ItemImage}
  #alias PrefEngine.ItemCardChangeset
  alias PrefEngine.SessionServer
  
  alias PrefUiWeb.PrefListComponents, as: C
  alias Phoenix.LiveView.JS

  @impl true
  def update(assigns, socket) do
    # NOTE: shouldn't current_user be part of session_vt (or at least the server)?
    %{id: id, session_vt: session_vt, current_user: current_user} = assigns

    # NOTE: can initialize this with data from session server
    {:ok, socket
      |> assign(:dialogue_open, false)
      |> assign(:id, id)
      |> assign(:session_vt, session_vt)
      |> assign(:current_user, current_user)
      # TODO: here we would like a changeset for a textfield with a list of URLs to add
      # TODO: would be nice to have a button somehow pop up for every pref level
      # this can be also used as a way to pick to which pref level to add the items
    }
  end

  ## Add Item ##
  def handle_event("add_item", _data, socket) do
    IO.puts "add_item"
    {:noreply, socket}
  end
end
