defmodule PrefUiWeb.PrefListComponents do
  use PrefUiWeb, :component

  alias PrefEngine.ItemImage

  defp item_card_width(title) do
    words = title
            |> String.split(" ")
            |> Enum.map(&String.length(&1))
    
    max = Enum.max words
    min = Enum.min words
    sum = Enum.sum words
    num = Enum.count words
    avg = sum/num

    if num >= 10 do
      floor(max*2+1)
    else
      floor(max+1)
    end
  end

  defp render_item(assigns, item, id, expand) do
    expand = (id == expand)

    name = if item.name do
      item.name
    else
      item.url
    end

    name = if name, do: String.slice(name, 0, 40), else: ""
    url  = item.url

    ~H"""
    <div draggable="true" class="draggable item" id={id}
    phx-click="toggle-item-expand" phx-value-id={id}>

    <%= if item.preview do %>
      <img class="item-img" src={"/item_img_preview/#{item.preview}"} />
    <% end %>
    <div class="item-text" style="height: 100%">
      <!--        <a href={url} target="_blank">-->
        <span class="material-icons handle" style="float: right; padding: 10px; background: rgba(1,1,1,0.1)">control_camera</span>
        <%= name %>
        <!--</a>-->
        <div style="display: flex">
          <%= if expand do %>
            <div>
              <span class="material-icons">link</span>
              <a href={url} target="_blank">
                <%= url %>
              </a>
            </div>
          <% end %>
        </div>
      </div>
    </div>
    """
  end

  def item(assigns) do
    item = assigns.item

    render_item(assigns, item, "item-#{item.id}", assigns.expand)
  end

  def pref(assigns) do
    pref = assigns.pref
    item = pref.item

    render_item(assigns, item, "pref-#{item.id}", assigns.expand)
  end

  def pref_level_mobile(assigns) do
    pref_level = assigns.pref_level
    item = List.first(pref_level.prefs).item
    name = if item.name do
      item.name
    else
      item.url
    end

    name = if name, do: name, else: ""

    ~H"""
    <div class="dropzone pref-level" id={"pref_level_mobile-#{pref_level.id}"}>
      <%= Enum.count(pref_level.prefs) %>
      <%= String.slice(name, 0, 20) %>
    </div>
    """
  end
end
