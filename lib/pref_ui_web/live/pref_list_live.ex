defmodule PrefUiWeb.PrefListLive do
  use PrefUiWeb, :live_view

  alias PrefUiWeb.NewItemModalComponent
  alias PrefUiWeb.NoobHelperModalComponent

  alias PrefEngine.Query
  alias PrefEngine.PrefList
  alias PrefEngine.{Repo, PrefList} # TODO: move to context module
  
  alias PrefUiWeb.PrefListComponents, as: C

  @impl true
  def mount(%{"pref_id" => pref_id}, %{"current_user" => current_user}, socket) do
    # convert to integer
    {pref_id, ""} = Integer.parse(pref_id)

    pl = Repo.get(PrefList, pref_id)
         |> Repo.preload([:user, [pref_levels: [prefs: :item]]])
    # TODO: check if pl is nil (i.e. this pref level doesn't exist)

    %{good: pref_good, bad: pref_bad, name: name} = PrefList.get_prefs(pl)
    items = PrefEngine.ItemSearch.search("", 0, 50)
            |> PrefEngine.ItemSearch.show_results

    Phoenix.PubSub.subscribe PrefEngine.PubSub, "pref_list:#{pref_id}"
    # TODO: also subscribe to every item to monitor changes of that item
    # TODO: also subscribe to every item recommendation

    {:ok, socket
      |> assign(:pref_id, pref_id)
      |> assign(:page_title, name)
      |> assign(:current_user, current_user)
      |> assign(:header_content, "_search_content.html")
      |> assign(:items, items)
      |> assign(:pref_good, pref_good)
      |> assign(:pref_bad, pref_bad)
      |> assign(:name, name) 
      |> assign(:query_changeset, Query.changeset(%Query{}))
      |> assign(:preflist_name_changeset,
        PrefList.name_changeset(%PrefList{}, %{name: name}))
      |> assign(:edit_preflist_name, false)
      |> assign(:expanded_item_id, false)
    }
  end

  ## PubSub handlers
  @impl true
  def handle_info({:update_preflist, _preflist_id}, socket) do
    pl = Repo.get(PrefList, socket.assigns.pref_id)
         |> Repo.preload([:user, [pref_levels: [prefs: :item]]])
    %{good: pref_good, bad: pref_bad, name: name} = PrefList.get_prefs(pl)

    {:noreply, socket
      |> assign(:pref_good, pref_good)
      |> assign(:pref_bad, pref_bad)
      |> assign(:name, name)
    }
  end

  @impl true
  def handle_event("search", %{"query" => %{"string_filter" => sf}}, socket) do
    # TODO: Also refactor ItemServer to something simpler
    items = PrefEngine.ItemSearch.search(sf, 0, 50)
            |> PrefEngine.ItemSearch.show_results

    {:noreply,
      socket
      |> assign(:items, items)
    }
  end

  @impl true
  def handle_event("dropped", params, socket) do
    %{
      "draggedId" => dragged_id,
      "dropzoneId" => dropzone_id
    } = params
    pref_id = socket.assigns.pref_id

    # get dragged id
    dragged_id = case dragged_id do
      "item-" <> item_id -> item_id
      "pref-" <> pref_id -> pref_id
    end
    {item_id, ""} = Integer.parse(dragged_id)

    case dropzone_id do
      "pref_good_add_at_start" ->
        IO.inspect({pref_id, item_id})
        PrefList.add_at_start(pref_id, :good, item_id)
      "pref_bad_add_at_start" ->
        PrefList.add_at_start(pref_id, :bad, item_id)
      "pref_level-" <> pref_level_id ->
        {pref_level_id, ""} = Integer.parse(pref_level_id)
        PrefList.add_into(pref_id, pref_level_id, item_id)
      "pref_level_add_after-" <> pref_level_id ->
        {pref_level_id, ""} = Integer.parse(pref_level_id)
        PrefList.add_after(pref_id, pref_level_id, item_id)
      x ->
        IO.inspect({:unknown, x})
    end

		{:noreply, socket}
  end

  ## Edit PrefList Name ##
  def handle_event("edit_preflist_name", _data, socket) do
    {:noreply, socket
      |> assign(:edit_preflist_name, !socket.assigns.edit_preflist_name)
    }
  end
  def handle_event("set_preflist_name", %{"pref_list" => %{"name" => name}}, socket) do
    case PrefList.update_name(socket.assigns.pref_id, name) do
      {:ok, _pl} ->
        {:noreply, socket
          |> assign(:edit_preflist_name, !socket.assigns.edit_preflist_name)
          |> assign(:page_title, name)
          |> assign(:name, name)
          |> assign(:preflist_name_changeset,
                    PrefList.name_changeset(%PrefList{}, %{name: name}))
        }
      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :preflist_name_changeset, changeset)}
    end
  end

  def handle_event("toggle-item-expand", %{"id" => id}, socket) do
    IO.puts id
    new_id = if socket.assigns.expanded_item_id == id do
      false
    else
      id
    end
    IO.puts new_id
    {:noreply, assign(socket, :expanded_item_id, new_id)}
  end
end
