defmodule PrefUiWeb.NoobHelperModalComponent do
  use PrefUiWeb, :live_component

  @impl true
  def update(assigns, socket) do
    # NOTE: shouldn't current_user be part of session_vt (or at least the server)?
    %{id: id, pref_id: pref_id, current_user: current_user} = assigns

    # NOTE: can initialize this with data from session server
    {:ok, socket
      |> assign(:dialogue_open, true)
      |> assign(:id, id)
      |> assign(:pref_id, pref_id)
      |> assign(:current_user, current_user)
    }
  end

  def handle_event("open_dialogue", _data, socket) do
    {:noreply, assign(socket, :dialogue_open, true)}
  end

  def handle_event("close_dialogue", _data, socket) do
    {:noreply, assign(socket, :dialogue_open, false)}
  end
end
