defmodule PrefUiWeb.ItemImageController do
  use PrefUiWeb, :controller

  alias PrefEngine.{Repo, ItemImage}

  def preview(conn, params) do
    case Repo.get(ItemImage, String.to_integer(params["img_id"])) do
      nil ->
        conn |> send_resp(404, "Image not found")
      img ->
        conn
        |> put_resp_content_type("image/webp")
        |> send_resp(200, img.preview)
    end
  end
end
