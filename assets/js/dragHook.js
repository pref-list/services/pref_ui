import { Draggable } from '@shopify/draggable';
import { default as Scrollable } from './drag/Scrollable'

export default {
  mounted() {
		if (this.drag) {
			this.drag.destroy();
		}

    //this.droppable = new Droppable(this.el, {
    //this.drag = new Draggable(this.el.querySelectorAll('.dropzone'), {
    this.drag = new Draggable(this.el.querySelectorAll('.dropzone'), {

      draggable: '.draggable',
      handle: '.handle',
      delay: {
        mouse: 10,
        drag: 10,
        touch: 5,
      },
      distance: 5, // it works! had to install @next version of draggable!
      mirror: {
        appendTo: 'body'
      },
    });
    // custom scroll-on-drag plugin
    this.drag.removePlugin(Draggable.Plugins.Scrollable);
    this.drag.addPlugin(Scrollable);

    // don't focus on input after drag (at least we don't want this on mobile)
    // oh, in fact, input shouldn't be focused on mobile at all!
    // TODO
    
    this.selected_container = null;
    var el = document.documentElement;

    this.drag.on('drag:start', () => {
      document.activeElement.blur()
    });
    this.drag.on('drag:move', () => {
      el.dataset.active_panel = "prefs"
    });
    this.drag.on('drag:over:container', (ev) => {
      this.selected_container = ev.data.overContainer;
    });
    this.drag.on('drag:out:container', (ev) => {
      this.selected_container = null
    });

		const hook = this;
		const selector = '#' + this.el.id;
    this.drag.on('drag:stop', (ev) => {

      if (this.selected_container && this.selected_container.id != 'items') {
				console.log(ev.data.source.id);
				console.log(this.selected_container.id);

        hook.pushEventTo(selector, 'dropped', {
          draggedId: ev.data.source.id,
          dropzoneId: this.selected_container.id,
        });
      }
    });
  },

	updated() {
		this.mounted();
	},
  destroyed() {
    if (this.drag) {
      this.drag.destroy();
    }
  }
};
