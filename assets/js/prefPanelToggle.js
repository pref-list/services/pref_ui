export default {
  toggle() {
    var el = document.documentElement;
    if (el.dataset.active_panel == "prefs") {
      el.dataset.active_panel = "items"
    } else { // either items or not set
      el.dataset.active_panel = "prefs"
    }
  } 
};
